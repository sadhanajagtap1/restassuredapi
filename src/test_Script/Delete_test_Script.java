package test_Script;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import CommonMethod.API_Trigger;

import CommonMethod.Utilities_Delete;
import io.restassured.response.Response;

public class Delete_test_Script  extends API_Trigger {

	public static void execute() throws IOException  {
		File logfolder = Utilities_Delete.creatFolder("D_Delete_API");
    	int statuscode =0;
    for (int i=0;i<5;i++) {
		Response resp = delete_API_Trigger(delete_request_body(), delete_endPoint());
		
		 statuscode=resp.statusCode();
		System.out.println(statuscode);				
		  if (statuscode==201) {
			   
			   Utilities_Delete.createLogFile("D_Delete_test_script",logfolder,post_endPoint(),resp.getHeaders().toString());
	        
	       break;
	        }
	        
	       else {
	        	System.out.println("Status code found in itration:" +i+ "is :" +statuscode+ ", andis not equal expected status code hesce retryoing ");
	        
	       }
	    }
	    	
	    	Assert.assertEquals(statuscode, 201,"Correct statuse code not found after rtrying for 5 times ");
	    }
	    
	}
		
