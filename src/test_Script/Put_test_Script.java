package test_Script;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethod.API_Trigger;
import CommonMethod.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_test_Script extends API_Trigger {

	public static void execute() throws IOException {
		
		File logfolder = Utilities.creatFolder("Put_API");
		
		int statuscode =0;
    	for (int i=0;i<5;i++) {
Response response= Put_API_Trigger(put_request_body(),put_endPoint());
		
		// step 3 extract status code
		
		statuscode=response.statusCode();
		System.out.println(statuscode);
	     if (statuscode==200) {
		// step 4 fetch response body parameter
		
		ResponseBody  responseBody=response.getBody();
		System.out.println(responseBody.asString());
		
		 Utilities.createLogFile("Put_test_script",logfolder,put_endPoint(),put_request_body(),response.getHeaders().toString(),responseBody.asString());
		 valiate(responseBody);
	        
	       break;
	        }
	        
	       else {
	        	System.out.println("Status code found in itration:" +i+ "is :" +statuscode+ ", andis not equal expected status code hesce retryoing ");
	        
	       }
	    }
	    	
	    	Assert.assertEquals(statuscode, 200,"Correct statuse code not found after rtrying for 5 times ");
	    }
	    
    	
	
	 public static void valiate(ResponseBody responseBody) {
		String res_name=responseBody.jsonPath().getString("name");
		String res_job=responseBody.jsonPath().getString("job");
		//String res_id=responseBody.jsonPath().getString("id");
		String res_updatedAt=responseBody.jsonPath().getString("updatedAt");
		res_updatedAt=res_updatedAt.toString().substring(0,11);

		// step 5 fetch requst body parameters
		
		JsonPath jsp_req= new JsonPath(put_request_body());
		String req_name=jsp_req.getString("name");
		String req_job=jsp_req.getString("job");
		
		// step 6 generate expected date
		
		LocalDateTime currentdate= LocalDateTime.now();
		String expecteddate=currentdate.toString().substring(0,11);
		
        // step 7 validate using TestNg Assertion
		
		Assert.assertEquals(res_name,req_name,"name should be not same");
		Assert.assertEquals(res_job,req_job,"job should be not same");
		//Assert.assertNotNull(res_id);
		Assert.assertEquals(res_updatedAt,expecteddate," creaed at in response body is not eual to date gengerted");
		
		
		

	}

}
