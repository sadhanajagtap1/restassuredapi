package test_Script;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;

import CommonMethod.API_Trigger;
import CommonMethod.Utilities;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Get_test_Script  extends API_Trigger {
	
	public static void execute() throws IOException {
		
		File logfolder = Utilities.creatFolder("Get_API");
		int statuscode =0;
    	for (int i=0;i<5;i++) {
		Response resp = get_API_Trigger(get_request_body(), get_endPoint());
		
		RestAssured.config/*RestAssured allows me to configure various aspects of mine API tests using this configuration object.*/ = RestAssuredConfig.config().sslConfig/* It allows you to configure SSL settings for your API requests*/(SSLConfig.sslConfig()/*RestAssured provides this factory method to create SSL configuration objects.*/.relaxedHTTPSValidation())/*It configures SSL validation to be relaxed, meaning it will accept all SSL certificates, including self-signed certificates, without validation. */;
		 statuscode = resp.statusCode();
		System.out.println(statuscode);
		
	     if (statuscode==201) {
	  

		// Step 3 : Build RequestSpecification
		RequestSpecification req_spec = RestAssured.given();

		//Step 3.1 : Trigger the API

		// Step 3.2 : Fetch the responsebody parameter
		ResponseBody responseBody = resp.getBody();
		System.out.println(responseBody.asString());

		Utilities.createLogFile("Get_test_script",logfolder,get_endPoint(),get_request_body(),resp.getHeaders().toString(),responseBody.asString());
		
		
		valiate(responseBody);
        
	       break;
	        }
	        
	       else {
	        	System.out.println("Status code found in itration:" +i+ "is :" +statuscode+ ", andis not equal expected status code hesce retryoing ");
	        
	       }
	    }
	    	
	    	Assert.assertEquals(statuscode, 201,"Correct statuse code not found after rtrying for 5 times ");
	    }
	 public static void valiate(ResponseBody responseBody) {
		
	  	 
			int exp_page = 2;
			int exp_per_page= 6;
			int exp_total =12;
			int exp_total_page= 2;
			 
			// Step 2.2 : Expected results
			int id[]= {7,8,9,10,11,12};
			String email[] = { "michael.lawson@reqres.in","lindsay.ferguson@reqres.in", "tobias.funke@reqres.in","byron.fields@reqres.in", "george.edwards@reqres.in","rachel.howell@reqres.in" };
			String first_name[] = { "Michael","Lindsay","Tobias","Byron","George", "Rachel"};
			String last_name[] = {"Lawson","Ferguson","Funke", "Fields","Edwards","Howell"};
			String avatar[] = {"https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg", "https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg"};	
			String exp_url = "https://reqres.in/#support-heading"; 
		 
		 
		 
		 
		 // Step 3.3 : Fetch The size of the data array (in the response body is determined.)
		List<String>dataArray = responseBody.jsonPath().getList("data"); 
		int sizeofarray = dataArray.size();

		// Step 4 : Use TestNG's Assert class is used to compare expected values with actual values retrieved from the API response.

		Assert.assertEquals(exp_page, exp_page);
		Assert.assertEquals(exp_per_page, exp_per_page);
		Assert.assertEquals(exp_total, exp_total);
		Assert.assertEquals(exp_total_page, exp_total_page);

		// Step 4.1 : statements compare expected values with actual values extracted from the response body using JSONPath expressions.
		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(Integer.parseInt(responseBody.jsonPath().getString("data[" + i + "].id")), id[i],
					"Validation of id failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].email"), email[i],"Validation of email failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].first_name"), first_name[i],"Validation of first_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].last_name"), last_name[i],"Validation of last_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].avatar"), avatar[i],"Validation of avatar failed for json object at index : " + i);
		}

		// Step 7.3 : Validate support json
		Assert.assertEquals(responseBody.jsonPath().getString("support.url"), exp_url, "Validation of URL failed");


				}
		}

		