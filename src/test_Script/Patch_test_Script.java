package test_Script;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethod.API_Trigger;
import CommonMethod.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_test_Script extends API_Trigger {
    public static void execute() throws IOException {
    	
    	       	File logfolder = Utilities.creatFolder("Patch_API");
        	
        	int statuscode =0;
        	for (int i=0;i<5;i++) {
            Response response = Patch_API_Trigger(patch_request_body(), patch_endPoint());

           int statusCode = response.getStatusCode();
            System.out.println("Status code: " + statusCode);
            
            if (statuscode==201) {

      
            ResponseBody responseBody = response.getBody();
            System.out.println("Response body: " + responseBody.asString());
            
            Utilities.createLogFile("Patch_test_script",logfolder,patch_endPoint(),patch_request_body(),response.getHeaders().toString(),responseBody.asString());
            
            valiate(responseBody);
            
            break;
             }
             
            else {
             	System.out.println("Status code found in itration:" +i+ "is :" +statuscode+ ", andis not equal expected status code hesce retryoing ");
             
            }
         }
         	
         	Assert.assertEquals(statuscode, 201,"Correct statuse code not found after rtrying for 5 times ");
         }
         
    public static void valiate(ResponseBody responseBody) {
            JsonPath jsonPath = responseBody.jsonPath();
            String res_name = jsonPath.getString("name");
            String res_job = jsonPath.getString("job");
            String res_updatedAt = jsonPath.getString("updatedAt");

            // Extract request body parameters
            JsonPath requestJsonPath = new JsonPath(patch_request_body());
            String req_name = requestJsonPath.getString("name");
            String req_job = requestJsonPath.getString("job");

            // Generate expected date
            LocalDateTime currentdate = LocalDateTime.now();
            String expecteddate = currentdate.toString().substring(0, 10);

            // Validate using TestNG Assertion
            Assert.assertEquals(res_name, req_name, "Name should be the same");
            Assert.assertEquals(res_job, req_job, "Job should be the same");
            Assert.assertNotNull(res_updatedAt, "UpdatedAt is null");
            Assert.assertEquals(res_updatedAt.substring(0, 10), expecteddate, "UpdatedAt date in response body is not equal to expected date");
    } 

}
