package trigger_Rest_Assured_API;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;


public class Post_API_Trigger_Refrence {

	public static void main(String[] args) {

		// Step 1 : Declare the needed variables 
		
		String BaseURI = "https://reqres.in";
		String requestBody = "{\r\n"
				+ "    \"email\": \"eve.holt@reqres.in\",\r\n"
				+ "    \"password\": \"pistol\"\r\n"
				+ "}";
		String hostname = "https://reqres.in";
		String resource = "/api/register";
		String headername = "Content-Type";
		String headervalue = "application/json";
				
		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());
		RestAssured.baseURI = BaseURI;
		int statuscode = given().header(headername,headervalue).body(requestBody).when().post(resource)
				.then().extract().statusCode();
		System.out.println(statuscode);
		
		
		// Step 2 : Configure the API for execution and log entire transaction (request
		
		// Step 3 : Configure the API for execution and save the response in a String

          RestAssured.baseURI = hostname; 
		
		String responseBody = given().header(headername, headervalue).body(requestBody).when().post(resource).then()
				.extract().response().asString();
		
		System.out.println(responseBody);

		// Step 4 : Parse the response body

		// Step 5 : Create the object of JsonPath
		JsonPath jsp_res = new JsonPath(responseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// Step 6 :Parse the request body using json Path

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Step 6.1 : Generate expected date

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Step 6.2 : Use TestNG's Assert

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, expecteddate, "updatedAt in ResponseBody is not equal to Date Generated");
	}

}


