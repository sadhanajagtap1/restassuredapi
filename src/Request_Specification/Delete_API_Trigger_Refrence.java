package Request_Specification;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;

public class Delete_API_Trigger_Refrence {

	public static void main(String[] args) {
		// Step 1 : Declare the needed variables

				String hostname = "https://reqres.in";
				String resource = "/api/users/2";

				RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());
				RestAssured.baseURI = hostname;

				given().log().all().when().delete(resource).then().log().all().extract().response().asString();
				
	}

}
