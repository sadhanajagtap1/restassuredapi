package Request_Specification;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class patch_API_Request_Specification {

	public static void main(String[] args) {
		//Step 1: Declare the needed variables
		
				String hostname="https://reqres.in";
				String resource="/api/users/2";
				String headername="Content-Type";
				String headervalue="application/json";
				String requestbody="{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"zion resident\"\r\n"
						+ "}";
				RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());

				// 2 Trigger Api
				
				//2.1 Build request specification using RequesrSpecification
				
				RequestSpecification req_spec= RestAssured.given();
				
				// step 2.2 set header
				
				req_spec.header(headername,headervalue);
				
				// step 2.3 set request body
				
				req_spec.body(requestbody);
				
				//step 2.4 Trigger the API
				
				Response response= req_spec.patch(hostname+resource);
				
				// step 3 extract status code
				
				int statuscode=response.statusCode();
				System.out.println(statuscode);
				
				// step 4 fetch response body parameter
				
				ResponseBody  responseBody=response.getBody();
				System.out.println(responseBody.asString());
				
				String res_name=responseBody.jsonPath().getString("name");
				String res_job=responseBody.jsonPath().getString("job");
				//String res_id=responseBody.jsonPath().getString("id");
				String res_updatedAt=responseBody.jsonPath().getString("updatedAt");
				res_updatedAt=res_updatedAt.toString().substring(0,11);

				// step 5 fetch requst body parameters
				
				JsonPath jsp_req= new JsonPath(requestbody);
				String req_name=jsp_req.getString("name");
				String req_job=jsp_req.getString("job");
				
				// step 6 generate expected date
				
				LocalDateTime currentdate= LocalDateTime.now();
				String expecteddate=currentdate.toString().substring(0,11);
				
		        // step 7 validate using TestNg Assertion
				
				Assert.assertEquals(res_name,req_name,"name should be not same");
				Assert.assertEquals(res_job,req_job,"job should be not same");
				//Assert.assertNotNull(res_id);
				Assert.assertEquals(res_updatedAt,expecteddate," creaed at in response body is not eual to date gengerted");
				
				
				

	}

}
