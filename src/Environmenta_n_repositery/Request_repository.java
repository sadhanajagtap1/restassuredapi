package Environmenta_n_repositery;

import java.io.IOException;
import java.util.ArrayList;

import CommonMethod.Utilities;

public class Request_repository extends Environmenta {

	public static String post_param_requestBody(String testcaseName) throws IOException {

		ArrayList<String> data = Utilities.ReadExcledata("Post_API", testcaseName);//we use ArrayList to trying to fetch the data from excel file
		String req_Name = data.get(1);
		String req_Job = data.get(2);
		String requestBody = "{\r\n" + "    \"name\": \"" + req_Name + "\",\r\n" + "    \"job\": \"" + req_Job
				+ "\"\r\n" + "}";

		return requestBody;

	}

	public static String post_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";

		return requestBody;
	}

	public static String put_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return requestBody;
	}

	public static String patch_request_body() {
		String requestbody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return requestbody;
	}

	public static String get_request_body() {
		String requestbody = "";
		return requestbody;
	}

	public static String delete_request_body() {
		String requestbody = "";
		return requestbody;
	}

}