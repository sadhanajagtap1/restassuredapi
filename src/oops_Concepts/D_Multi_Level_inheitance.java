package oops_Concepts;

public class D_Multi_Level_inheitance extends C_Single_Level_inheritance {
	String batterytype;
	
	public void battery() {
		System.out.println("Start the mobile phone :"+company+" and model :"+model);
	}

	public static void main(String[] args) {
		D_Multi_Level_inheitance batteryofmob = new D_Multi_Level_inheitance ();
		
		batteryofmob.company = "Cellular One";
		batteryofmob.model = "Motorola-DynaTAC";
		batteryofmob.startMobile();
		batteryofmob.swichOffit();
		batteryofmob.dialnumber();
		batteryofmob.batterytype = "Chargeable";
		batteryofmob.battery();
	}

}
