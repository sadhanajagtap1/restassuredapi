
package oops_Concepts;

public interface Name_Change_Requirement {
	int min_age = 18;

	public void Oldname();

	public void Newname();

}
