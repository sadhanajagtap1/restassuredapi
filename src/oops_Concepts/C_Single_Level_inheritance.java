package oops_Concepts;

public class C_Single_Level_inheritance extends A_Class_Object {	
	
	public String company;
	  String model;
	 // int cost;
	  
	 // 5 to 16 Encapsulation 
	  public void startMobile() {
		System.out.println("Start the mobile phone :"+company+" and model :"+model);
	  }
	  
	  public void swichOffit() {
		System.out.println("Switch of the mobile phone :"+company+" and model :"+model);
	  }
	
	public void dialnumber() {
		System.out.println("Start the mobile phone :"+company+" and model :"+model);
	}
	
	public static void main(String[] args) {
		
		C_Single_Level_inheritance dialno = new C_Single_Level_inheritance ();
		dialno.company = "Cellular One";
		dialno.model = "Motorola-DynaTAC";
		dialno.startMobile();
		dialno.swichOffit();
		dialno.dialnumber();

	}

}
