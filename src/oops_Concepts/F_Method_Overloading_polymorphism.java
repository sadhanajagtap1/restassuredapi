package oops_Concepts;

public class F_Method_Overloading_polymorphism {

	public void runSpeed(String Mobile) {
		System.out.println("Slow");
	}
	
	public void runSpeed(double Mobile) {
		System.out.println("Fast");
	}

	public static void main(String[] args) {
		F_Method_Overloading_polymorphism overLoad = new F_Method_Overloading_polymorphism();
		overLoad.runSpeed("Samsung");
		overLoad.runSpeed(10);
		
	}

}
