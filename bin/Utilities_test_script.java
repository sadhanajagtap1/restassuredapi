package test_Script;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import org.testng.Assert;

import CommonMethod.API_Trigger;
import CommonMethod.Utilities;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Utilities_test_script extends API_Trigger {

    public static void execute() throws IOException {
    	
    	File logfolder = Utilities.creatFolder("Utilities_API");
    	int statuscode =0;
    	for (int i=0;i<5;i++) {
        Response resp = Post_API_Trigger(post_request_body(), post_endPoint());
        RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());
   
        statuscode = resp.statusCode();
        System.out.println(statuscode);
        
        if (statuscode==200) {
        	
        ResponseBody responseBody = resp.getBody();
        System.out.println(responseBody.asString());

        Utilities.createLogFile("Utilities_test_script",logfolder,post_endPoint(),post_request_body(),resp.getHeaders().toString(),responseBody.asString());
     
        valiate(responseBody);
        
        break;
        }
        
        else {
        	System.out.println("Status code found in itration:" +i+ "is :" +statuscode+ ", andis not equal expected status code hesce retryoing ");
        
        }
    	}
    	
    	Assert.assertEquals(statuscode, 200,"Correct statuse code not found after rtrying for 5 times ");
    }
    
    public static void valiate(ResponseBody responseBody) {
        String res_name = responseBody.jsonPath().getString("name");
        String res_job = responseBody.jsonPath().getString("job");
        String res_id = responseBody.jsonPath().getString("id");
        String res_createdAt = responseBody.jsonPath().getString("createdAt");
        res_createdAt = res_createdAt.substring(0, 10); // Trim to just date part

        JsonPath jsp_req = new JsonPath(post_request_body());
        String req_name = jsp_req.getString("name");
        String req_job = jsp_req.getString("job");

        // Get current date in UTC timezone
        LocalDateTime currentDateTime = LocalDateTime.now(ZoneOffset.UTC);
        LocalDate currentDate = currentDateTime.toLocalDate();
        String expecteddate = currentDate.format(DateTimeFormatter.ISO_DATE);

        Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
        Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
        Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
        Assert.assertEquals(res_createdAt, expecteddate, "CreatedAt date mismatch");
    }
}
